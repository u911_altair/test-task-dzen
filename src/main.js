import Vue from 'vue';
import VueRouter from 'vue-router';


//Pages
import Orders from './pages/orders.vue';
import Products from './pages/products.vue';

browserDetected();

Vue.use(VueRouter);

const routesList = [
	{ path: '/orders', name: 'Orders', component: Orders },
	{ path: '/products', name: 'Products', component: Products },
];

const router = new VueRouter({
	mode: 'history',
	routes: routesList
});

new Vue({
	el: "#app",
	router: router
});

// for cross-browser compatibility

function browserDetected() {

	let browser = 'unknown';
	let body = document.body;
	let isOpera = (!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	let isFirefox = typeof InstallTrigger !== 'undefined';
	let isIE = /*@cc_on!@*/false || !!document.documentMode;
	let isEdge = !isIE && !!window.StyleMedia;
	let isChrome = !!window.chrome && !!window.chrome.webstore;
	let is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
	let isTouch = !!('ontouchstart' in window) || !!('msmaxtouchpoints' in window.navigator);

	if (isTouch) {
		body.classList.add('is-touch');
	}

	if (isOpera) {
		browser = 'is_opera';
	}

	if (isFirefox) {
		browser = 'firefox';
	}

	if (isIE) {
		browser = 'ie';
	}

	if (isChrome) {
		browser = 'chrome';
	}

	if (isEdge) {
		browser = 'edge';
	}

	if (is_safari) {
		browser = 'safari';
	}

	// window.browser = browser;
	body.classList.add('browser-' + browser);
}

var setVh = function() {
	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);
};

setVh();
window.addEventListener('resize', function() {
	setVh();
});

browserDetected();

