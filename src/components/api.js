
export default {
	orders: [
		{
			id: 1,
			title: 'Длинное название прихода',
			date: '2017-03-29 12:09:33',
			description: 'desc',
			getProducts: {},
		},
		{
			id: 2,
			title: 'Длинное название прихода',
			date: '2017-06-29 12:09:33',
			description: 'desc',
			getProducts: {},
		},
		{
			id: 3,
			title: 'Длинное название прихода',
			date: '2017-09-29 12:09:33',
			description: 'desc',
			getProducts: {},
		},
		{
			id: 4,
			title: 'Длинное название прихода',
			date: '2017-11-29 12:09:33',
			description: 'desc',
			getProducts: {},
		}
	],
	products: [
		[
			{
				id: 1,
				serialNumber: 'sn-12.3496789',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 1,
				date: '2017-06-29 12:09:33',
				representative: 'Семен Семенов'
			},
			{
				id: 2,
				serialNumber: 'sn-12.3456789',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Tv',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id: 3,
				serialNumber: 'sn-12.3556789',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id:4,
				serialNumber: 'sn-12.3455789',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366)',
				type: 'Tv',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 200, symbol: 'USD', isDefault: 0},
					{value: 5200, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			}
		],
		[
			{
				id: 5,
				serialNumber: 'sn-12.3489567',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 300, symbol: 'USD', isDefault: 0},
					{value: 7200, symbol: 'UAH', isDefault: 1}
				],
				order: 1,
				date: '2017-06-29 12:09:33',
				representative: 'Семен Семенов'
			},
			{
				id: 6,
				serialNumber: 'sn-12.3489567',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Tv',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id: 7,
				serialNumber: 'sn-12.3489567',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Tv',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id:8,
				serialNumber: 'sn-12.3489567',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366)',
				type: 'Monitors',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 200, symbol: 'USD', isDefault: 0},
					{value: 5200, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			}
		],
		[
			{
				id: 9,
				serialNumber: 'sn-12.7893456',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 2000, symbol: 'USD', isDefault: 0},
					{value: 52000, symbol: 'UAH', isDefault: 1}
				],
				order: 1,
				date: '2017-06-29 12:09:33',
				representative: 'Семен Семенов'
			},
			{
				id: 10,
				serialNumber: 'sn-12.7893456',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id: 11,
				serialNumber: 'sn-12.7893456',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id:12,
				serialNumber: 'sn-12.7893456',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366)',
				type: 'Monitors',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 200, symbol: 'USD', isDefault: 0},
					{value: 5200, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			}
		],
		[
			{
				id: 13,
				serialNumber: 'sn-12.1111111',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 1,
				date: '2017-06-29 12:09:33',
				representative: 'Семен Семенов'
			},
			{
				id: 14,
				serialNumber: 'sn-12.1111111',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 100, symbol: 'USD', isDefault: 0},
					{value: 2600, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id: 15,
				serialNumber: 'sn-12.1111111',
				isNew: 1,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366) 6 X58-USB3',
				type: 'Monitors',
				specification: 'Flat',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 1000, symbol: 'USD', isDefault: 0},
					{value: 26000, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			},
			{
				id:16,
				serialNumber: 'sn-12.1111111',
				isNew: 0,
				photo: 'pathToFile.jpg',
				title: 'Gigabyte Technology X58-USB3(Socet 1366)',
				type: 'Monitors',
				specification: 'Specification 1',
				guarantee: {
					start: '2017-06-29 12:09:33',
					end: '2017-06-29 12:09:33'
				},
				price: [
					{value: 200, symbol: 'USD', isDefault: 0},
					{value: 5200, symbol: 'UAH', isDefault: 1}
				],
				order: 2,
				date: '2017-06-29 12:09:33',
				representative: ''
			}
		]
	],
};