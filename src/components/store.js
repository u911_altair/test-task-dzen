import Vue from 'vue'
import Vuex from 'vuex'
import $api from '@/components/api.js';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		openModalOrder: false,
		ordersData: {},
		productsData: [],
		productsType: [],
		productsSpec: []
	},
	getters: {
		getOrdersData: () => {
			for (let i = 0; i < $api.orders.length; i++) {
				$api.orders[i].getProducts = $api.products[i]
			}

			return $api.orders;
		},
		getProductsData: (state) => {
			for (let i = 0; i < $api.products.length; i++) {

				for (let j = 0; j < $api.products[i].length; j++) {
					state.productsData.push($api.products[i][j])
				}
			}

			return state.productsData;
			
		},
		getProductsType: (state) => {
			let typeOfProducts = state.productsType;
			let mass = state.productsData;

			for (let i = 0; i < mass.length; i++) {
				typeOfProducts.push(mass[i].type)
			}

			typeOfProducts.sort(function(a, b){
				let nameA = a, nameB = b;

				if (nameA < nameB) {
					return -1
				} else if (nameA > nameB) {
					return 1
				} else {
					return 0
				}
			})

			for (let i = 0; i < typeOfProducts.length; i++) {

				for (let j = i + 1; j < typeOfProducts.length;) {

					if (typeOfProducts[i] == typeOfProducts[j]) {
						typeOfProducts.splice(j, 1);
					} else {
						j++;
					}
				}	
			}

			typeOfProducts.unshift('all');

			return typeOfProducts;
		},
		getProductsSpec: (state) => {
			let specOfProducts = state.productsSpec;
			let mass = state.productsData;

			for (let i = 0; i < mass.length; i++) {
				specOfProducts.push(mass[i].specification)
			}

			specOfProducts.sort(function(a, b){
				let nameA = a, nameB = b;

				if (nameA < nameB) {
					return -1
				} else if (nameA > nameB) {
					return 1
				} else {
					return 0
				}
			})

			for (let i = 0; i < specOfProducts.length; i++) {

				for (let j = i + 1; j < specOfProducts.length;) {

					if (specOfProducts[i] == specOfProducts[j]) {
						specOfProducts.splice(j, 1);
					} else {
						j++;
					}
				}	
			}

			specOfProducts.unshift('all');

			return specOfProducts;
		}
	},
	mutations: {
		setOpenModalOrder(state, openModalOrder){
			state.openModalOrder = openModalOrder;
		}
	}
});